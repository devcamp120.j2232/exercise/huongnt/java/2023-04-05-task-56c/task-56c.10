package com.devcamp.task56.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.task56.countryregionapi.model.Country;
import com.devcamp.task56.countryregionapi.model.Region;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class CountryService {

    public ArrayList<Region> getRegionsList() {
        return this.regionsList;
    }

    public void setRegionsList(ArrayList<Region> regionsList) {
        this.regionsList = regionsList;
    }
    public void setCountries(ArrayList<Country> countries) {
        this.countries = countries;
    }

    private ArrayList<Region> regionsList = new ArrayList<Region>();

    public CountryService() {
                //Khởi tạo Viêt Nam
                Country obj1 = new Country("VN","Việt Nam");
                Region reg11 = new Region("HN","Hà Nội");
                Region reg12 = new Region("SG","Sài Gòn");
                Region reg13 = new Region("DN","Đà Nẵng");
                Region reg14 = new Region("CT","Cần Thơ");
                Region reg15 = new Region("HU","Huế");
                Region reg16 = new Region("QN","Quảng Ninh");
                Region reg17 = new Region("BĐ","Bình Định");
                ArrayList<Region> regions1 = new ArrayList<Region>();
                regions1.add(reg11);
                regions1.add(reg12);
                regions1.add(reg13);
                regions1.add(reg14);
                regions1.add(reg15);
                regions1.add(reg16);
                regions1.add(reg17);
                obj1.setRegions(regions1);
                
                //Khởi tạo Campuchia
                Country obj2 = new Country("CA","Campuchia");
                Region reg21 = new Region("HN2","Cam Hà Nội");
                Region reg22 = new Region("SG2","Cam Sài Gòn");
                Region reg23 = new Region("DN2","Cam Đà Nẵng");
                Region reg24 = new Region("CT2","Cam Cần Thơ");
                Region reg25 = new Region("HU2","Cam Huế");
                Region reg26 = new Region("QN2","Cam Quảng Ninh");
                Region reg27 = new Region("BĐ2","Cam Bình Định");
                ArrayList<Region> regions2 = new ArrayList<Region>();
                regions2.add(reg21);
                regions2.add(reg22);
                regions2.add(reg23);
                regions2.add(reg24);
                regions2.add(reg25);
                regions2.add(reg26);
                regions2.add(reg27);
                obj2.setRegions(regions2);
                
                //Khởi tạo Lào
                Country obj3 = new Country("LA","Lào");
                Region reg31 = new Region("HN3","Lào Hà Nội");
                Region reg32 = new Region("SG3","Lào Sài Gòn");
                Region reg33 = new Region("DN3","Lào Đà Nẵng");
                Region reg34 = new Region("CT3","Lào Cần Thơ");
                Region reg35 = new Region("HU3","Lào Huế");
                Region reg36 = new Region("QN3","Lào Quảng Ninh");
                Region reg37 = new Region("BĐ3","Lào Bình Định");
                ArrayList<Region> regions3 = new ArrayList<Region>();
                regions3.add(reg31);
                regions3.add(reg32);
                regions3.add(reg33);
                regions3.add(reg34);
                regions3.add(reg35);
                regions3.add(reg36);
                regions3.add(reg37);
                obj3.setRegions(regions3);
                
                this.countries.add(obj1);
                this.countries.add(obj2);
                this.countries.add(obj3);
                //Tạo 1 list chứa tất cả region
                regionsList.addAll(regions1);
                regionsList.addAll(regions2);
                regionsList.addAll(regions3);
    }
    private ArrayList<Country> countries = new ArrayList<Country>();

     public ArrayList<Country> getCountries() {
        return countries;
     }

    public Country getCountry( String countryCode) {
        Country countryResult = null;
        for (int counter = 0; counter < countries.size(); counter++) { 		      
            Country countryCheck = countries.get(counter);
            if (countryCode.equalsIgnoreCase(countryCheck.getCountryCode())){
                countryResult = countryCheck;
            }
        }
        return countryResult;
    }
}

package com.devcamp.task56.countryregionapi.model;

import java.util.ArrayList;

public class Country {


    public Country(String countryCode, String countryName, ArrayList<Region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regions = regions;
    }

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public Country() {
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return this.countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<Region> getRegions() {
        return this.regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }
    private String countryCode;
    private String countryName;
    private ArrayList<Region> regions;
}

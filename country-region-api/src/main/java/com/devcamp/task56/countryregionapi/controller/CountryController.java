package com.devcamp.task56.countryregionapi.controller;
import org.springframework.web.bind.annotation.*;
import com.devcamp.task56.countryregionapi.service.*;
import com.devcamp.task56.countryregionapi.model.*;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class CountryController {
    @Autowired
    private CountryService countryService;

    @CrossOrigin
	@GetMapping("/countries")
	public ArrayList<Country> getCountries() {
			return countryService.getCountries();
	}
    @CrossOrigin
    @GetMapping("/country-info/{code}")
	public Country getCountryInfo(@PathVariable String code) {
        return countryService.getCountry(code);
	}
}

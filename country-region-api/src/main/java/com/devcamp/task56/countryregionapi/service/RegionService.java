package com.devcamp.task56.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import com.devcamp.task56.countryregionapi.model.Region;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class RegionService {

    @Autowired
    private CountryService countryService;

    public ArrayList<Region> getRegions() {
        return this.regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }
    private ArrayList<Region> regions = new ArrayList<Region>();

    public Region getRegionByCode( String regionCode) {
        this.regions = countryService.getRegionsList();
        Region regionResult = null;
        for (int counter = 0; counter < regions.size(); counter++) { 		      
            Region regionCheck = regions.get(counter);
            if (regionCode.equalsIgnoreCase(regionCheck.getRegionCode())) {
                regionResult = regionCheck;
            }
        }  
        return regionResult;
    }
}
